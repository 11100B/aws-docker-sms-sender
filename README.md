./mvnw clean install
docker build -t aws-docker-sms-sender .
docker image ls
docker run -d -p 8081:8081 aws-docker-sms-sender
docker container ls
docker container exec -it <<ID>> sh
# hit "Ctrl + p + q" to safe exit terminal
docker container stop <<ID>>
